extends KinematicBody2D

signal sheep_exploded

export var speed : int = 25
var direction : Vector2
var rng = RandomNumberGenerator.new()
var screen_size  # Size of the game window.
var bomb_countdown


# Initilize the sheep
func _ready():
  hide()
  bomb_countdown = 15
  rng.randomize()
  screen_size = get_viewport_rect().size


# Use timeout to decide on direction and animation
func _on_Timer_timeout():
  if (bomb_countdown != 0):
    # Pick a movement direction
    var random_number = rng.randf()
    if random_number < 0.05:
      direction = Vector2.ZERO
    elif random_number < 0.8:
      direction = Vector2.DOWN.rotated(rng.randf() * 2 * PI)

    if (random_number < 0.99999) and (random_number > 0.98):
      $Sheep_Bleat1.play()
    elif (random_number < 0.98) and (random_number > 0.96):
      $Sheep_Bleat2.play()
    elif (random_number < 0.96) and (random_number > 0.94):
      $Sheep_Bleat3.play()
    
    bomb_countdown -= 1
    pick_sheep_animation()
  else:
    pass


# Move sheep about
func _physics_process(delta):
  if (bomb_countdown != 0):
    move_and_collide((direction * speed * delta))
    position.x = clamp(position.x, 50, screen_size.x-50)
    position.y = clamp(position.y, 35, screen_size.y-35)

    if direction.normalized().x >= -0.1:
      $AnimatedSprite.flip_h = true
    else:
      $AnimatedSprite.flip_h = false
  else:
    pass


# Unmake the sheep bomb
func _on_TouchScreenButton_pressed():
  if (bomb_countdown != 0):
    bomb_countdown += 5
    bomb_countdown = clamp(bomb_countdown, 0, 15)
    pick_sheep_animation()
  else:
    pass


# Decide on which sheep animation to play
func pick_sheep_animation():
  match bomb_countdown:
    11, 12, 13, 14, 15:
      $AnimatedSprite.play("default")
    8, 9, 10:
      $AnimatedSprite.play("blink_bomb1")
      speed = 75
    5, 6, 7:
      $AnimatedSprite.play("blink_bomb2")
      speed = 50
    1, 2, 3, 4:
      $AnimatedSprite.play("blink_bomb3")
      speed = 25
    0:
      $AnimatedSprite.play("explosion")
      emit_signal("sheep_exploded")


# Set bomb_countdown to zero
func set_bomb_countdown_to_zero():
  if (bomb_countdown == 0):
    pass
  else:
    bomb_countdown = 0
    $AnimatedSprite.play("delayed_explosion")
