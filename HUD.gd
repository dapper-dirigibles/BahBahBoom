extends CanvasLayer


# Messages to show before game start
func show_message(text):
  $MessageLabel.text = text
  $MessageLabel.show()
  $MessageTimer.start()


# Messages to show at game over
func show_game_over():
  show_message("Game Over")
  yield($MessageTimer, "timeout")
  $MessageLabel.text = "Bah-Bah\nBoom!"
  $MessageLabel.show()
  yield(get_tree().create_timer(1), "timeout")
  $SupportUsButton.show()


# Show score
func update_score(score):
  $ScoreLabel.text = str(score)


# Hide message
func _on_MessageTimer_timeout():
  $MessageLabel.hide()


# Call browser when button is pressed
func _on_SupportUsButton_pressed():
  OS.shell_open("https://www.patreon.com/dapperdirigibles")


# Call to hide SupportUsButton
func hide_support_us_button():
  $SupportUsButton.hide()


# Call browser when button is pressed
func _on_LegalLink_pressed():
  OS.shell_open("https://gitlab.com/dapper-dirigibles/BahBahBoom/-/blob/master/Privacy_Policy.md")

# Call to hide LegalLink
func hide_legal_link_button():
  $LegalLink.hide()
