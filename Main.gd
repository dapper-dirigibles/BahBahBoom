extends Node

export(PackedScene) var sheep_scene
var score
var screen_size


func _ready():
  randomize()


# Called on explosion
func game_over():
  $ScoreTimer.stop()
  $SheepTimer.stop()
  $HUD.show_game_over()
  $Music.stop()
  $ClockTickingSound.stop()
  $ExplosionSound.play()
  $GameOverTimer.start()
  get_tree().call_group("sheep_group", "set_bomb_countdown_to_zero")


# Various functions tasks for when a new game starts
func new_game():
  score = 0
  $StartTimer.start()
  $HUD.update_score(score)
  $HUD.show_message("Get Ready")
  $HUD.hide_legal_link_button()
  $MusicStartTimer.start()
  $ClockTickingSound.play()
  $ExplosionSound.stop()


# Sheep spawn timer
func _on_SheepTimer_timeout():
  $HUD.hide_support_us_button()
  var sheep = sheep_scene.instance()
  add_child(sheep)
  sheep.show()
  sheep.position = $SheepSpawnPosition.position
  sheep.speed = 150
  sheep.direction = Vector2.RIGHT
  sheep.bomb_countdown = 15
  sheep.pick_sheep_animation()
  sheep.connect("sheep_exploded", self, "game_over")


# Keep track of score
func _on_ScoreTimer_timeout():
  score += 1
  $HUD.update_score(score)


# Slight delay before actual game start
func _on_StartTimer_timeout():
  $SheepTimer.start()
  $ScoreTimer.start()


# Autostarts the game
func _on_AutoStartTimer_timeout():
  new_game()


# Ensures the "Game Over" screen stays up
func _on_GameOverTimer_timeout():
  get_tree().call_group("sheep_group", "queue_free")
  new_game()


# Helps to times the music to start just as the first sheep spawns
func _on_MusicStartTimer_timeout():
  $Music.play()
